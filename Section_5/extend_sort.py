even = [22, 2, 4, 12]
odd = [9, 15, 3, 1]

# Use the extend method
even.extend(odd)
print(even)
''' This method concatenate  both lists'''

# Use the sort method and add another list
another_list = even
print(another_list)
''' This output prints the same as above since concatenates both lists '''

# Use the sort() method
even.sort()
print(even)
''' This sort output sorts the values from least to greatest '''

# Use sort() method but with reverse order
even.sort(reverse=True)
print(even)
''' This sort() outputs the values from greatest to least '''

print('Great job, you did somthing!')