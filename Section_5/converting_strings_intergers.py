# This practice file is to show how to convert a list of strings to integers

lotto_numbers = [
                '900', '950',
                '356', '23',
                '420', '986'
]
values = ' '.join(lotto_numbers)
print(values) 
# Prints strings separated by spaces. DO NOT PUT A COMMA IN .JOIN. IT WILL RUIN DAYS
# 900 950 356 23 420 986

new_values = values.split()
print(new_values) 
# Prints a list with each string inside
# ['900', '950', '356', '23', '420', '986']

converted_strings = []
for strings in new_values:
    converted_strings.append(int(strings))
print(converted_strings) 
# Prints a list with converter strings
# [900, 950, 356, 23, 420, 986]
