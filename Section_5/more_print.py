name = "Tim"
age = 10

print(name, age, "Python", 2020)
print((name, age, "Python", 2020)) # Parentheses must be used when you pass a tuple literal as an argument to a function














#print(name, age, "Python", 2020)
# In documentation, the function signature contains parameters within the print()
# The first parameter is '*objects' and the objects are in the print statement

# print(name, age, "Python", 2020, sep=', ')
# The second parameter in the print() is 'sep=' '(space)
# The print statement inserted commas in the output (Tim, 10, Python, 2020)