# This practice file is to show how to convert a list of strings to integers

lotto_numbers = [
                '900', '950',
                '356', '23',
                '420', '986'
]
new_strings = ' '.join(lotto_numbers)
print(new_strings)
# Prints strings separated by spaces
# 900 950 356 23 420 986

more_strings = new_strings.split()
print(more_strings)
# Prints a list with each string inside
# ['900', '950', '356', '23', '420', '986']

conversion_strings = []
for strings in more_strings:
    conversion_strings.append(int(strings))
print(conversion_strings)    
# Prints a list with converted strings
# [900, 950, 356, 23, 420, 986]