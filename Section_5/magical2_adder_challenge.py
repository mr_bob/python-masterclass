#====================================================================================================
# Write a python program which prompts the user to enter three integers separated by ","
# The user input is of the form: a, b, c where a, b and c are numbers
# Use the split() and join() method
#====================================================================================================

# Ask user to input 3 numbers
user_input = input("Please enter three numbers so that we can calulate a + b - c:\n")

# Create empty lists
string_split = []
string_join = []

# Check if commas or spaces in user_input
if ',' not in user_input:
    string_join = ''.join(user_input)
    string_split = string_join.split()
else:
    string_split = user_input.split(',')

# Use a for loop to iterate through list
for index in range(len(string_split)):
    string_split[index] = int(string_split[index]) # Converting strings to integers

# Define a, b, c for calcualtion
a, b, c = string_split
result =  a + b - c
print("The calculation of a + b - c is", result)


