a = b = c = d = e = f = 42
print(c)
#=====================================================================
# This is 'unpacking' a tuple
#=====================================================================
x, y, z = 1, 2, 76
print(x)
print(y)
print(z)
print()

print("Unpacking a tuple")

data = 1, 2, 76 # data represents a tuple
x, y, z = data
print(x)
print(y)
print(z)

print("Unpacking a list")

data_list = [12, 13, 14]
data_list.append(15)
#=====================================================================
# You cannot append to tuples. Another advantage of tuples is you unpack them successfully
# Because tuple cannot change, you will know how many items there are to unpack
# You can unpack any sequence type
#=====================================================================
p, q, r = data_list
print(p)
print(q)
print(r)