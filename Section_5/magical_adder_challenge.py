#====================================================================================================
# Write a python program which prompts the user to enter three integers separated by ","
# The user input is of the form: a, b, c where a, b and c are numbers
# Use the split() and join() method
#====================================================================================================

# This is one way to code the challenge but if the user doesnt enter commas, program will crash
print()
print('Lets figure out what a + b - c is based on your input!\n')
user_input = input("Enter three numbers separated by commas: \n")
numbers = user_input.split(',')

for index in range(len(numbers)):
    numbers[index] = int(numbers[index])

a, b, c = numbers
result = a + b - c    # Lines 15-16 are another way of printing result
#print("The total calculation is", numbers[0] + numbers[1] - numbers[2])
print("The total calculation is", result)

#====================================================================================================

user_input = input("Enter three numbers so that we can calculate a + b - c: \n")


string_join = []
string_list = []

# Check if commas are used or not. Create two lists just in case user enters commas or not
if ',' not in user_input:
    string_join = ''.join(user_input) # If user did not, use join to create join list
    string_list = string_join.split() # Then separate values using split

else:
    string_list = user_input.split(',') # If user used commas, split values after each comma

# Loop is iterates through the list of numbers in string_list
for index in range(len(string_list)):
    string_list[index] = int(string_list[index])
a, b, c = string_list # Defining a, b, c as the numbers in the string_list
results = a + b - c # Calculating all 3 values
print("The calculation of a + b - c is", results)







