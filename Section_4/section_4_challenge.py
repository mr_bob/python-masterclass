# print a number of options for the user to choose from

# print("Please choose one of the following options: "
#        "\n1. Learn Python"
#        "\n2. Learn Java"
#        "\n3. Learn API"
#        "\n4. Have Dinner"
#        "\nENTER 0 to QUIT: ")

# while True:
#     choice = int(input())
#     if choice == 0:
#         print("You have chosen to quit")
#         break
#     if choice in range(1, 5):
#         print("You chose option {}, now choose another.".format(choice))
#     else:
#         print("Please choose one of the following options: "
#        "\n1. Learn Python"
#        "\n2. Learn Java"
#        "\n3. Learn API"
#        "\n4. Have Dinner"
#        "\nENTER 0 to QUIT: ")