# data = [104, 101, 4, 105, 308, 103, 5,   # This code is the same as outliers.py. This is a better way to code
#         107, 100, 306, 106, 102, 108]
# data = [104, 105, 110, 120, 130, 130, 150,
#         160, 170, 183, 185, 187, 188, 191] # This list does not have outliers
# data = [4, 5, 104, 105, 110, 120, 130, 130, 150,
#         160, 170, 183, 185, 187, 188, 191, 350, 360] # This list contains outliers 
data = [4, 5, 104, 105, 110, 120, 130, 130, 150,
        160, 170, 183, 185, 187, 188, 191] # This list contains outliers
min_valid = 100
max_valid = 200

# for index in range(len(data) - 1, - 1, - 1):
#     if data[index] < min_valid or data[index] > max_valid:
#         print(index, data)
#         del data[index]
# print(data)       
#  
''' lines 6-10 is the same code as lines 12-16 '''

top_index = len(data) - 1
for index, value in enumerate(reversed(data)):
    if value < min_valid or value > max_valid:
        print(top_index - index, value)
        del data[top_index - index]
print(data)





# data = [104, 101, 4, 105, 308, 103, 5,
#         107, 100, 306, 106, 102, 108]
# min_valid = 100
# max_valid = 200

# for index in range(len(data) - 1, - 1, - 1):
#     if data[index] < min_valid or data[index] > max_valid:
#         print(index, data)
#         del data[index]

