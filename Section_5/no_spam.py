# Print out all meals  in the menu but with 'spam' removed.
# Remove 'spam' from each menu, then print the list
# Print out the items in each list as long as its not 'spam'
# You can write two sets of code using both approaches
menu = [
    ["egg", "bacon"], 
    ["egg", "sausage", "bacon"], 
    ["egg", "spam"], 
    ["egg", "bacon", "spam"], 
    ["egg", "bacon", "sausage", "spam"], 
    ["spam", "bacon", "sausage", "spam"],  
    ["spam", "sausage", "spam", "bacon", "spam", "tomato", "spam"],
    ["spam", "egg", "spam", "spam", "bacon", "spam"],
]

# for meal in menu:
#     for index in range(len(meal) -1, -1, -1):
#         if meal[index] == 'spam':
#             del meal[index]

#     print(", ".join(meal)

''' lines 16-25 are the same code '''

for meal in menu:
    for item in meal:
        if item != 'spam':
            print(item, end=' ')
    print()  

print('------------------------------')

names = [
    ['bobby', 'alex', 'gia', 'fart'],
    ['niko', 'liza', 'haven', 'fart'],
    ['fart', 'nyla', 'rex', 'colby'],
    ['yoshi', 'fart','nala', 'dani'], 
]

# for gas in names:
#     for item in gas:
#         if item != 'fart':
#             print(item, end=', ') # Using end= adds the item on one line instead of making a new line
#     print() 

for gas in names:
    items = ', '.join((item for item in gas if item != 'fart')) # This is a Generator Expression
    print(items)     

print('------------------------------')

# for poop in names:
#     for index in range(len(poop) -1, -1, -1):
#         if poop[index] == 'fart':
#             del poop[index]
#     print(poop)            

# print('------------------------------')

# coffee = [
#     ['vanilla', 'yellow mellow', 'espresso'],
#     ['peppermint', 'yellow mellow'],
#     ['yellow mellow', 'caramel macchiato', 'french vanilla']
# ]

# for cafe in coffee:
#     for item in cafe:
#         if item != 'yellow mellow':
#             print(item)
#     print()            

# for cafe in coffee:
#     for index in range(len(cafe) -1, -1, -1):
#         if cafe[index] == 'yellow mellow':
#             del cafe[index]
#     print(cafe)            

