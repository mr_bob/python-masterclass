shopping_list = ['milk', 'pasta', 'eggs', 'spam', 'bread', 'rice']

for item in shopping_list:
    if item == 'spam':
        break
    print('Buy ' + item)

print('------------------')

bands_ive_seen = ['tool', 'pennywise', 'lagwagon', 'hanson', 'bad religion']

for band in bands_ive_seen:
    if band == 'hanson':
        print('Ive never seen ' + band.title() +' before.')
        continue
    print('Ive seen ' + band.title() + ' before.')
