for i in range(10, 0, -2):
    print('i is now {}'.format(i))

print('-' * 80)

for number in range(0, 21):
    print('Number is {}'.format(number))    
print('-' * 80)

for things in range(0, 10):
    print('The number is {}'.format(things))
print('-' * 80)

names = 'Alex'
for letters in names:
    print('Spelling Names: {}'.format(letters))  
print('-' * 80)
father = 'Bobby'
for char in father:
    print('Spell fathers name: {}'.format(father))      