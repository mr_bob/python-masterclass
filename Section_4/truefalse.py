day = 'Saturday'
temp = 30
raining = False
# Because day is False and temp is True = False
# Then 'not raining' is False too

if (day == 'Friday' and temp > 27) or not raining:
    print('Go swimming!')
else:
    print('Learn python!')  

print()
food = 'steak'
quanity = 5
hot_sauce = False

if food == 'steak' and quanity > 2 or not hot_sauce:
    print('Lets eat')
else:
    print('Maybe next time') 

if 0:
    print('True')
else:
    print('False')

name = input('Please enter your name: ')
# if name:
if name != '':
    print('Hello, {}'.format(name))
else:
    print('Are you the man with no name?')     