flowers = [
    'Daffodil',
    'Evening Primrose',
    'Hydrangea',
    'Iris',
    'Lavander',
    'Sunflower',
    'Tiger Lily',
]

# for flower in flowers:
#     print(flower)

separator = ' | '
output = separator.join(flowers)
print(output)
    # Or 

print(' | '.join(flowers))

print('--------------------')

names = [
    'Alex',
    'Bobby',
    'Gia',
    'Haven',
    'Niko',
    'Liza',
]

print(' | '.join(names))
print('--------------------')

foods = [
    'Burger',
    'Fries',
    'Pizza',
    'Milk',
    'Eggs'
]

print(', '.join(foods))
