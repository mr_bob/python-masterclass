data = [101, 2, 30, 178, 180, 201]

min_valid = 100
max_valid = 250
print(data)
# for index in range(len(data) - 1, - 1, - 1):
#     if data[index] < min_valid or data[index] > max_valid:
#         print(index, data)
#         del data[index]
# print(data)        

top_index = len(data) - 1
for index, value in enumerate(reversed(data)):
    if value < min_valid or value > max_valid:
        print(top_index - index, value)
        del data[top_index - index]
print(data)        

















# even = [2, 6, 4, 12, 10]
# odd = [3, 9, 13, 15, 31]

# numbers = even + odd
# print(numbers)

# new_list = sorted(numbers)
# print(new_list)

# digits = sorted('109876543210')
# print(digits)

# digits = list('109876543210')
# print(digits)

# more_numbers = list(numbers) # This is a new list that prints out the same original (nunmbers) list
# print(more_numbers)

# more_numbers = numbers.copy()
# print(more_numbers)

# print(numbers is more_numbers)
# print(numbers == more_numbers)




# statement = 'The dog is quacky and doesnt like the zoo fool'
# letters = sorted(statement)
# print(letters)

# numbers = [1, 6, 10, 7, 3, 2, 4]
# sorted_numbers = sorted(numbers)
# print(sorted_numbers)

# numbers.sort()
# print(numbers)

# another_statement = sorted('The dude claimed to be the dude',
#                             key=str.casefold)
# print(another_statement)                            

# names = ['gia',
#         'Haven',
#         'alex',
#         'Bobby',
#         'liza', 
#         'NIKO']
# names.sort()
# print(names)        

# names.sort(reverse=True) # This output prints out backwards
# print(names) 

# names.sort(key=str.casefold) # This output prints out the names in alphabetical order
# print(names) 




# available_parts = ['computer',
#                     'monitor',
#                     'keyboard',
#                     'mouse',
#                     'hdmi cable',
#                     'dvd drive'
#                     ]

# valid_choices = []
# for i in range(1, len(available_parts) + 1):
#     valid_choices.append(str(i))
# print(valid_choices)

# current_choice = ('-')
# computer_parts = [] # Creates an empty list

# while current_choice != '0':
#     if current_choice in valid_choices:
#         print(f"Adding {current_choice}")
#         index = int(current_choice) - 1
#         chosen_part = available_parts[index]
#         computer_parts.append(chosen_part)
#     else:
#         print("Please add options from the list below: ")
#         for number, part in enumerate(available_parts): 
#             print("{0}: {1}".format(number + 1, part))
#     current_choice = input()

# print(computer_parts)