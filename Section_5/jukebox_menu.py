from nested_data import albums

# while True: # This while loop prints all the info. Songs prints in a format  of [list(tuple)]
#     print("Please choose your album (invalid choice exits): ")
#     for index, (title, artist, year, songs) in enumerate(albums):
#         print('{}: {}, {}, {}, {}'.format(index + 1, title, artist, year, songs))

#     break   
SONG_LIST_INDEX = 3 # All CAPS means its a Constant and should never be changed. 
SONG_TITLE_INDEX = 1 # Its also the index within the tuple

while True: # This while loop prints all the info. Songs prints [list(tuple)]
    invalid_choice = True
    while invalid_choice:
        print("Please choose your album (invalid choice exits): ")
        for index, (title, artist, year, songs) in enumerate(albums):
            print('{}: {}'.format(index + 1, title))

        choice = int(input())
        if 1 <= choice <= len(albums):
            invalid_choice = False
            songs_list = albums[choice - 1][SONG_LIST_INDEX]
        else:
            print("\nInvalid choice: ") 

    invalid_choice = True
    while invalid_choice:        
        print("Please choose your song: ")
        for index, (track_number, song) in enumerate(songs_list): # songs_list is coming from line 19 and this is called "Unpacking Tuple"
            print('{}: {}'.format(index + 1, song))

        song_choice = int(input())
        if 1 <= song_choice <= len(songs_list): # The songs are from line 24
            invalid_choice = False
            title = songs_list[song_choice - 1][SONG_TITLE_INDEX]
        else:
            print("\nInvalid choice: ")     

    print("\nPlaying {}".format(title))
    print('=' * 40) 
    break   

    # print(albums[choice - 1]) # These prints were to show what line 19 is doing (Indexing)
    # print(song_list)
    # print()