#         012345678901234  
parrot = "Norwegian Blue"

print(parrot[0:6:2])
print(parrot[0:6:3])


number = '9,223;372:036 854,775;807'
separators = number[1::4]
print(separators)

values = "".join(char if char not in separators else " " for char in number).split()
print([int(val) for val in values])


# print(parrot[0:6]) # Norweg
# print(parrot[-14:-8])
# print(parrot[-4:-2]) # Bl
# print(parrot[-4:12]) # Bl


# print(parrot[3:5])
# print(parrot[0:9])
# print(parrot[:9])
# print(parrot[10:])
# print(parrot[:6] + parrot[6:])
# print(parrot[:])


# letters = 'abcdefghijklmnopqrstuvwxyz'

# print(letters[0:4] + letters[12:15])


# print(parrot[3])
# print(parrot[4])
# print()
# print(parrot[3])
# print(parrot[6])
# print(parrot[8])

# print()

# print(parrot[-11])
# print(parrot[-1])
# print()
# print(parrot[-11])
# print(parrot[-8])
# print(parrot[-6])
# print()


# girl = 'alex chavarria'

# print(girl[0].title())
# print(girl[1].title())
# print(girl[2].title())
# print(girl[3].title())