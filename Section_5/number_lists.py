empty_list = []
even = [2, 4, 6, 8]
odd = [1, 3, 5, 7, 9]

numbers = [even, odd] # This output prints out a list with two lists
print(numbers)

for number_list in numbers:
    print(number_list)

    for value in number_list:
        print(value)







# numbers = [even + odd]
# print(numbers) # This output concatenates both lists

# sorted_numbers = sorted(numbers)
# print(sorted_numbers)
# ''' This creates a new list and sorts numbers in order '''
# print(numbers) # This output prints the original variable (numbers)

# digits = sorted('432985617')
# print(digits)
# ''' This output sorts the 'string' in numerical order while also creating a new list'''
#                 # ['1', '2', '3', '4', '5', '6', '7', '8', '9']

digits = list('432985617')
print(digits)
''' Using (list) prints out the string in same order'''

# # more_numbers = list(numbers)
# # more_numbers = numbers[:]
# more_numbers = numbers.copy()
# print(more_numbers) # Lines 23-25 all do the same thing. Diff options to code with
# ''' This output creates a new list and copies the (numbers) list same as above'''
#                 # [2, 4, 6, 8, 1, 3, 5, 7, 9]

# print(numbers is more_numbers)
# ''' This output is False because they are not the same list even though they have the same values'''
# print(numbers == more_numbers)
# ''' This output is True cause they have the same values in both lists '''

#**********************************************************************************************************

# even.extend(odd) 
# print(even)
# ''' The extend func combines values in both lists 
#     Output is [2, 4, 6, 8, 1, 3, 5, 7, 9]'''

# another_even = even
# print(another_even)
# ''' Adding another list gives same output
#     Output is [2, 4, 6, 8, 1, 3, 5, 7, 9]
#               [2, 4, 6, 8, 1, 3, 5, 7, 9] '''

# even.sort(reverse=True)
# print(even)
# print(another_even)
# ''' This sorts in reverse order and prints out the (even) list 
#     Output is [9, 8, 7, 6, 5, 4, 3, 2, 1]
#               [9, 8, 7, 6, 5, 4, 3, 2, 1]'''

# print(min(even))
# print(max(even))
# print(min(odd))
# print(max(odd))

# print()
# print(len(even))
# print(len(odd))
# print()

# print("mississippi".count('iss'))

# even.append(10)
# print(even)