shopping_list = ["milk", "pasta", "eggs", "spam", "bread", "rice"]

item_to_find = "spam"
found_at = None

# for index in range(6):
# for index in range(len(shopping_list)):
#     if shopping_list[index] == item_to_find:
#         found_at = index
#         break

if item_to_find in shopping_list:
    found_at = shopping_list.index(item_to_find) 

if found_at is not None:
    print("Item found at position {}".format(found_at))
else:
    print('{} not found'.format(item_to_find))  

print('----------------------')

bands = ['tool', 'pennywise', 'face to face', 'bad religion']

band_to_find = 'tool'
found_at = None

if band_to_find in bands:
    found_at = bands.index(band_to_find)

if found_at is not None:
    print('Band found at position {}'.format(found_at))
else:
    print('{} not found'.format(band_to_find))           