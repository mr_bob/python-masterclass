albums = [('Welcome to my Nightmare', 'Alice Cooper', 1975),
        ('Bad Company', 'Bad Company', 1974),
        ('Nightflight', 'Budgie', 1981),
        ('More Mayhem', 'Emilda May', 2011),
        ('Ride the Lightning', 'Metallica', 1984),
        ]
#================================================================
# If there is only [], that is a list
# By adding the () around each album within the [], it is now considered a tuple, not list
#================================================================
# print(len(albums))

for album in albums:
    print("Album: {}, Artist: {}, Year: {}"
            .format(album[0], album[1], album[2])) # This is unpacking the tuple using index
print('================================================================')
#================================================================
# Fix errors by unpacking the tuples into the variables
# Two ways of doing it, 1: Try to do it by only changing one line of code
# Added line 23 to get same output and avoided printing out the indexes
#================================================================
for album in albums:
    name, artist, year = album
    print("Album: {}, Artist: {}, Year: {}"
            .format(name, artist, year)) 
print('================================================================')

#================================================================
# A more efficient way. An advantage is that you also have the original tuples
#================================================================
for name, artist, year in albums:
    print("Album: {}, Artist: {}, Year: {}"
            .format(name, artist, year))

#================================================================
# Homogeneous = of the same kind
# Heterogeneous = diverse in character or content (2 strings, 1 interger)
#================================================================
