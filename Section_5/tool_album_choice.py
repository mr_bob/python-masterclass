from tool_project import tool_albums

SONG_LIST_INDEX = 3
SONG_TITLE_INDEX = 1

while True:
    input_invalid = True
    while input_invalid: # This is a nested while loop
        print('Please choose a Tool album to listen to: ')
        for index, (artist, title, year, songs) in enumerate(tool_albums):
            print('{}: {} - {}'.format(index + 1, title, year))
        choice = int(input())
        if 1 <= choice <= len(tool_albums):
            input_invalid = False
            songs_list = tool_albums[choice - 1][SONG_LIST_INDEX]
        else:
            print("Invalid input")
    
    invalid_song = True
    while invalid_song:
        print("\nPlease choose a song to jam: ")
        for index, (track_number, song) in enumerate(songs_list):
            print('{}: {}'.format(index + 1, song)) # The {} is called string formatting
        song_choice = int(input())
        if 1 <= song_choice <= len(songs_list):
            invalid_song = False
            title = songs_list[song_choice - 1][SONG_TITLE_INDEX]  
        else:
            print("\nInvalid input")

    print('\nNow playing {}.'.format(title))
    print('=' * 80) 
    break          
