# panagram = '''The quick brown
# fox jumps\tover
# the lazy dog'''

# words = panagram.split()
# print(words)

# print('--------------------')

# numbers = '9,223,372,036,854,775,807'
# print(numbers.split(','))

# print('--------------------')
# print('--------------------')

#values = "".join(char if char not in separators else " " for char in numbers).split()
# generated_list = ['9', ' ',
#                   '2', '2', '3', ' ',
#                   '3', '7', '2', ' ',
#                   '0', '3', '6', ' ',
#                   '8', '5', '4', ' ',
#                   '7', '7', '5', ' ',
#                   '8', '0', '7']
# values = ''.join(generated_list)
# print(values)

# values_list = values.split()
# print(values_list)


# print('--------------------')
# Use a for loop to produce a list of ints, rather than strings.
# You can either modify the contents of the 'values_list' list in place,
# # or create a new list of ints.


# cars = [
#         'dodge', 'ram', ' ',
#         'ford', 'ranger', ' ',
#         'jeep', 'explorer']

# items = ' '.join(cars) # The join() separates each string by whatever I tell it. This case its a space
# print(items)

# new_items = items.split()
# print(new_items)

# print('--------------------')

# foods = [
#         'pizza', 'pepperoni', 'bacon',
#         'burger', 'cheese', 'bacon',
#         'eggs', 'bacon', 'cheese'
# ]

# grub = ', '.join(foods)
# print(grub)

# more_grub = grub.split()
# print(more_grub)



generated_list = ['9', ' ',
                  '2', '2', '3', ' ',
                  '3', '7', '2', ' ',
                  '0', '3', '6', ' ',
                  '8', '5', '4', ' ',
                  '7', '7', '5', ' ',
                  '8', '0', '7']

print()
# Use a for loop to produce a list of ints, rather than strings.
# You can either modify the contents of the 'values_list' list in place,
# or create a new list of ints.

values = ''.join(generated_list)
print(values) # Output is the strings separated by spaces

print('--------------------')

values_list = values.split()
print(values_list) # Output is a list with concatenated strings of each line

print('--------------------')

for index in range(len(values_list)):
    values_list[index] = int(values_list[index])
print(values_list)    
# Advantage to above method, it replaces the data and saves memory
# As oppossed to making another list like lines 97-100 

# for strings in values_list:
#     converted_strings = int(strings)
#     print(converted_strings) # Output prints integers but not in a list

converted_strings = []
for strings in values_list:
    converted_strings.append(int(strings))
print(converted_strings) # Output converts the strings into integers and puts them in a list
