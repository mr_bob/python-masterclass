tool_albums = [
    ('Tool', 'AEnima', 1996,
    [
        (1, 'Stinkfist'),
        (2, 'Eulogy'),
        (3, 'H.'),
        (4, 'Forty-Six & 2'),
        (5, 'Message To Harry Manback*'),
        (6, 'Hooks With A Catch'),
        (7, 'Jimmy'),
        (8, 'Die Eier Von Satan'),
        (9, 'Pushit'),
        (10, 'AEnema'),
        (11, 'Third Eye'),
    ]
    ),
    ('Tool', 'Lateralus', 2001,
    [
        (1, 'The Grudge'),
        (2, 'The Patient'),
        (3, 'Schism'),
        (4, 'Parabol'),
        (5, 'Parabola'),
        (6, 'Ticks And Leeches'),
        (7, 'Lateralus'),
        (8, 'Disposition'),
        (9, 'Reflection'),
        (10, 'Faaip De Oiad'),
    ]
    ),
    ('Tool', 'Fear Inoculum', 2019,
    [
        (1, 'Fear Inoculum'),
        (2, 'Pneuma'),
        (3, 'Litanie contre la Peur'),
        (4, 'Invincible'),
        (5, 'Legion Inoculant'),
        (6, 'Descending'),
        (7, 'Culling Voices'),
        (8, 'Chocolate Chip Trip'),
        (9, '7empest'),
        (10, 'Mockingbeat'),
    ]
    ),
]  

# for artist, title, year, songs in tool_albums:
#     print('\nArtist: {} \nAlbum: {} \nYear: {} \nTracks: {}'.format(artist, title, year, songs))
# print()

# print(tool_albums[2]) # All info
# print(tool_albums[2][0]) # Print Artist
# print(tool_albums[2][1]) # Print album name
# print(tool_albums[2][2]) # Print year
# print(tool_albums[2][3]) # Print songs
