# This is going to be a practice file for while loops by guessing a number
import random

highest_number = 1000
answer = random.randint(1, highest_number)
quitting = 0

print(answer)

# Tell the user to guess a number between 1 and 10
print("Please guess a number between 1 and {}: ".format(highest_number))
print("If you'd like to quit, press 0 at anytime: ")

guess = int(input())
if guess == answer:
    print("Well done! You guessed correctly!")

while guess != answer:
    if guess == quitting:
        print("Thanks for trying quitter!")
        break
    if guess < answer:
        print('Guess higher: ')
    else:
        print("Guess lower: ")
    guess = int(input())
    if guess == answer:
        print("Good job!")
            