menu = [
    ["egg", "bacon"], 
    ["egg", "sausage", "bacon"], 
    ["egg", "spam"], 
    ["egg", "bacon", "spam"], 
    ["egg", "bacon", "sausage", "spam"], 
    ["spam", "bacon", "sausage", "spam"],  
    ["spam", "sausage", "spam", "bacon", "spam", "tomato", "spam"],
    ["spam", "egg", "spam", "spam", "bacon", "spam"],
]

for meal in menu:
    for index in range(len(meal) -1, -1, -1):
        if meal[index] == 'spam':
            del meal[index]      

    print(', '.join(meal))

''' lines 16-25 are the same code '''

# for meal in menu:
#     for item in meal:
#         if item != 'spam':
#             print(item, end=', ')
#     print() 
# This for loop leaves a trailing comma in each list

# for meal in menu:
#     items = ', '.join((item for item in meal if item != 'spam'))
#     print(items)
# This for loop removes the trailing comma       