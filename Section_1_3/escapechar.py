split_string = "This string have been\nsplit over\nseveral\nlines"
print(split_string)

tabbed_string = "1\t2\t3\t4\t"
print(tabbed_string)

print('The pet shop owner said "No, no, \'e\'s uh,...he\'s resting".')

print("The pet shop owner said \"No, no, 'e's,...he's resting\".")

print("""The pet owner said "No, no, \
'e's uh,...he's resting. """)
print()
another_split_string = """This string has been \
split over \
several \
lines"""

print(another_split_string)

print("C:\\Users\\timbuchalka\\notes.txt")
print(r"C:\Users\timbuchalka\notes.txt")