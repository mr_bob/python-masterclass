for i in range(1, 13):
    for j in range(1, 13):
        print('{0} times {1} is {2}'.format(i, j, i * j))
    print('--------------')

for number in range(1, 5):
    for numbers in range(1, 5):
        print('{0} plus {1} is {2}'.format(number, numbers, number + numbers))
    print('-' * 50)        



# names = 'AlEx ChAVaRRia'

for capitals in names:
    if capitals.isupper():
        print(capitals)
    elif capitals.islower():
        print(capitals)        
print('-' * 80)

things = 'bIke SKAteboard CAr'

for capitals in things:
    if capitals.isupper():
        print(capitals)
    elif capitals.islower():
        print(capitals)
print('---------------')           