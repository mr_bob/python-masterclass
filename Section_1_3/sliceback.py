letters = 'abcdefghijklmnopqrstuvwxyz'

backwards = letters[25:0:-1]
print(backwards)
print()

backwards = letters[25::-1]
print(backwards)
print()

# create a slice that produces the characters qpo

print(letters[16:13:-1])
print()
print(letters[4::-1])
print()
print(letters[25:17:-1])

print()
names = 'bobbyvaldez'
print(names[10::-1])
print(names[0:5])
print(names[4::-1])

