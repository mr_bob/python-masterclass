albums = [
    ("Welcome to my Nightmare", "Alice Cooper", 1975,
     [
         (1, "Welcome to my Nightmare"),
         (2, "Devil's Food"),
         (3, "The Black Widow"),
         (4, "Some Folks"),
         (5, "Only Women Bleed"),
     ]
     ),
    ("Bad Company", "Bad Company", 1974,
     [
         (1, "Can't Get Enough"),
         (2, "Rock Steady"),
         (3, "Ready for Love"),
         (4, "Don't Let Me Down"),
         (5, "Bad Company"),
         (6, "The Way I Choose"),
         (7, "Movin' On"),
         (8, "Seagull"),
     ])
]

# Print both albums
for album, artist, year, songs in albums:
    print(album, artist, year, songs)
print()

# Print out one album
album = albums[0]
print(album) # This [1] index is the 2nd Tuple() in the nested list
print()

''' The rest of the code is reading from line 31 since that is the specific album being called'''
# Print out the songs to the album
songs = album[3]
print(songs)
print()

# Print out one song from the album
song = songs[1]
print(song)


# unknown = "Unknown Road", 'Pennywise', 1995
# bad = 'Generator', 'Bad Religion', 1998
# fx = 'Linoleum', 'NOFX', 1996

# title, artist, year = fx
# print("The award for best song goes to", artist, title, year)
# print('========================')

# table = ('Coffee table', 50, 100, 46)
# name, length, width, height = table
# print('The total measurement is', length + width + height)

# print('========================')

# kids = ('Gia', 16)
# person, age = kids

# print(person, age)