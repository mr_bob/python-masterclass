# print('Today is a good day to learn python')
# print("python is fun")
# print("Python's string are easy to use")
# print('We can even include "quotes" in strings')
# print("Hello" + " world")
# greeting = "Hello"
# name = 'Bob'
# print(greeting + name)

# # if we want a space, we can add that too
# print(greeting + ' ' + name)


age = 24
print(age)

# print(type(greeting))
# print(type(age))

# age_in_words = '2 years'
# print(name + f" is {age} years old")
# print(type(age))

print(f'Pi is approximately {22/7:12.50f}')
pi = 22/7
print(f'Pi is approximately {pi:12.50f}')

# names = ['alex', 'bobby']

# print(names[0].title() + ' is with ' + names[1].title())
# print()
# payment = 200
# car = '$20,000'
# print(f'My monthly payment is {payment}. The care cost me {car}.')

# age = 55
# print(f'My goal is to have at least 3 properties by the age {age}!')

flower = "rose"
colour = "red"
print('The {0} is {1}. The car is also {1}.'.format(flower, colour))

data = "1:A, 2:B, 3:C, 4:D, 5:E, 6:F, 7:G, 8:H"
print(data[1:5])