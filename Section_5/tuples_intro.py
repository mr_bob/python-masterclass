welcome = 'Welcome to my Nightmare', 'Alice Cooper', 1975
bad = 'Bad Company', 'Bad Company', 1974
budgie = 'Nightflight', 'Budgie', 1981
imelda = 'More Mayhem', 'Emilda May', 2011
metallica = 'Ride the Lightning', 'Metallica', 1984
#=====================================================================
# Indexing a tuple works the same as indexing a string or a list by positioning the index in []
# Unlike lists, a tuple is immutable. If you try to change a tuple, it'll error out
# Tuples dont have the overhead of the methods needed to change them. Tuples use less memory than lists. 
# If dealing with millions of these things, you can save memory by using tuples
#=====================================================================
# print(metallica)
# print(metallica[0]) 
# print(metallica[1])
# print(metallica[2])

#=====================================================================
#This is an easier way to know what is being unpacked in the tuple
#=====================================================================
title, artist, year = metallica
print(title)
print(artist)
print(year)

#=====================================================================
# Lines 28-29 is the harder way. You would have to know what index they are
#=====================================================================
table = ("Coffee table", 200, 100, 75, 34.50)
print(table[1] * table[2])

#=====================================================================
# Lines 34-35 is easier to read. 
#=====================================================================
name, length, width, height, price = table
print(length * width)

# metallica2 = list(metallica) 
# print(metallica2)
#=====================================================================
# By assigning list to the variable, it is now a list and mutable
# Using Tuples helps prevent bugs in your programs
#=====================================================================












# t = ('a', 'b', 'c') # A tuple is like a list but uses parentheses() instead
# print(t)