# This is practicing if, elif and else statements

lotto = 1000000

# Tell the person to play the lotto by guessing the winning number
print()
print("Lets play a game of lotto. \nGuess the winning number and you'll be rich!"
        '\nWanna play?: ')
answer = input().casefold()
while answer != 'yes' and answer != 'no':
    print('Please type yes or no')
    answer = input().casefold()
    
if answer == 'no': # Need to figure out how to keep script going with lower or uppercase input
    print("You weren't gonna win anyways ya degenerate gambler!")
    

if answer == 'yes': # Need to figure out how to keep script going with lower or uppercase input
    print("\nGreat! Pick One: $100, $1,000 or $1,000,000")
    print('\nHowever, only type in the numbers: ')
    guess = int(input())
    if guess == lotto:
            print('Can you hear Post Malone "Congradulations" playing in your head? '
                    '\nCause you just won $1,000,000!!')
                   
    if guess < lotto:
        print('Cheap ass, try again and think BIG: ')
        guess = int(input())
        if guess == lotto:
            print("Now we're talking!")
        else:
            print('You call yourself a gambler?! Go home!')