# for index, character in enumerate("abcdefgh"):
#     print(index, character)
print()

for t in enumerate('abcdefgh'):
    print(t)
print()
#=====================================================================
# By using the built-in function enumerate(), the output will print a tuple for each value
# Tuples may be constructed in a number of ways:
# Using a pair of parentheses to denote the empty tuple: ()
# Using a trailing comma for a singleton tuple: a, or (a,)
# Separating items with commas: a, b, c or (a, b, c)
# Using the tuple() built-in: tuple() or tuple(iterable)
#=====================================================================

for t in enumerate('abcdefgh'):
    index, character = t
    print(index, character)
print()


index, character = (0, 'a')
print(index)
print(character)    

# The enumerate method helps with identifying the indexes is needed